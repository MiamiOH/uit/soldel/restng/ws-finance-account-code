<?php

return [
    'resources' => [
        'finance' => [
            \MiamiOH\WSFinanceAccountCode\Resources\AccountIndexResourceProvider::class,
            \MiamiOH\WSFinanceAccountCode\Resources\AccountResourceProvider::class
        ],
    ]
];
