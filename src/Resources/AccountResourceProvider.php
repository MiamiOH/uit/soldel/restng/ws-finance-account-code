<?php


namespace MiamiOH\WSFinanceAccountCode\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSFinanceAccountCode\Services\Account\Service;

class AccountResourceProvider extends ResourceProvider
{
    private $name = 'accountCode';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.get.model',
            'type' => 'object',
            'properties' => [
                'title' => [
                    'type' => 'string'
                ],
                'accountCode' => [
                    'type' => 'string'
                ],
                'chartOfAccountsCode' => [
                    'type' => 'string'
                ],
                'accountTypeCode' => [
                    'type' => 'string'
                ],
                'nextChangeDate' => [
                    'type' => 'string'
                ],
                'dataEntryIndicator' => [
                    'type' => 'string'
                ],
                'statusIndicator' => [
                    'type' => 'string'
                ]
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get finance account code',
            'name' => $this->name . '.get.single',
            'service' => $this->name,
            'tags' => ['finance'],
            'method' => 'getSingle',
            'pattern' => '/finance/accountCode/v1',
            'options' => [
                'accountCode' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return only the account records that match Account code'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'An account code.',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
                App::API_FAILED => [
                    'description' => 'Failed to retrieve account code.',
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Get finance account code typeahead',
            'name' => $this->name . '.get.typeahead',
            'service' => $this->name,
            'tags' => ['finance'],
            'method' => 'getTypeAhead',
            'isPageable' => true,
            'pattern' => '/finance/accountCode/typeahead/v1',
            'options' => [
                'q' => [
                    'type' => 'single',
                    'required' => false,
                    'description' => 'Return only the account records that match account code or title'
                ],
                'chartOfAccountsCode' => [
                    'type' => 'single',
                    'required' => false,
                    'description' => 'Return only the account records that match Chart of Accounts Code'
                ],
                'accountTypeCode' => [
                    'type' => 'single',
                    'required' => false,
                    'description' => 'Return only the account records that match Account Type Code'
                ],
                'nextChangeDate' => [
                    'type' => 'single',
                    'required' => false,
                    'description' => 'Return only the account records that match Next Change Date. Date format:\'YYYY - MM - DD\''
                ],
                'dataEntryIndicator' => [
                    'type' => 'single',
                    'required' => false,
                    'description' => 'Return only the account records that match Data Entry Indicator'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'An account code.',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
                App::API_FAILED => [
                    'description' => 'Failed to retrieve account code.',
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
