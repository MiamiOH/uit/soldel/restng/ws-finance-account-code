<?php

namespace MiamiOH\WSFinanceAccountCode\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSFinanceAccountCode\Services\AccountIndex\Service;

class AccountIndexResourceProvider extends ResourceProvider
{
    private $name = 'accountIndexCode';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.get.model',
            'type' => 'object',
            'properties' => [
                'title' => [
                    'type' => 'string'
                ],
                'accountIndexCode' => [
                    'type' => 'string'
                ],
                'accountCode' => [
                    'type' => 'string'
                ],
                'fundCode' => [
                    'type' => 'string'
                ],
                'organiztionCode' => [
                    'type' => 'string'
                ],
                'programCode' => [
                    'type' => 'string'
                ],
                'chartOfAccountsCode' => [
                    'type' => 'string'
                ]
            ]
        ]);
    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database source',
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Get finance account index code',
            'name' => $this->name . '.get.single',
            'service' => $this->name,
            'tags' => ['finance'],
            'method' => 'getSingle',
            'pattern' => '/finance/accountIndexCode/v1',
            'options' => [
                'accountIndexCode' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Account index code'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'An account index code.',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
                App::API_FAILED => [
                    'description' => 'Failed to retrieve account index code.',
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Get finance account index code typeahead',
            'name' => $this->name . '.get.typeahead',
            'service' => $this->name,
            'tags' => ['finance'],
            'method' => 'getTypeAhead',
            'isPageable' => true,
            'pattern' => '/finance/accountIndexCode/typeahead/v1',
            'options' => [
                'q' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Account index code or title'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'An account index code.',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/' . $this->name . '.get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
                App::API_FAILED => [
                    'description' => 'Failed to retrieve account index code.',
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {
    }
}
