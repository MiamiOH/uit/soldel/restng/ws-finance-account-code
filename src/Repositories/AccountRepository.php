<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/4/18
 * Time: 4:50 PM
 */

namespace MiamiOH\WSFinanceAccountCode\Repositories;

interface AccountRepository
{
    public function readByCodeTypeahead(string $queryString, string $coasCode, string $aTypeCode, string $nChgDate, string $dataEntryInd, bool $typeahead = false, int $limit = 20): array;
    public function readByCode(string $queryString, bool $typeahead = false, int $limit = 20): array;
}
