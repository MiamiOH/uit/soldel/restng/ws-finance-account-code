<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/4/18
 * Time: 4:50 PM
 */

namespace MiamiOH\WSFinanceAccountCode\Repositories;

interface AccountIndexRepository
{
    public function readByIndexCode(string $queryString, bool $typeahead = false, int $limit = 20): array;
}
