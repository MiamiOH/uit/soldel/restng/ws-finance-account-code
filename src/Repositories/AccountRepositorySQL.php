<?php

namespace MiamiOH\WSFinanceAccountCode\Repositories;

use MiamiOH\WSFinanceAccountCode\DataTransformers\DataTransformer;
use MiamiOH\WSFinanceAccountCode\EloquentModels\AccountEloquentModel;
use function var_dump;

/**
 * Class AccountRepositorySQL
 */
class AccountRepositorySQL implements AccountRepository
{
    /**
     * @var DataTransformer|null
     */
    private $dataTransformer = null;

    /**
     * AccountRepositorySQL constructor.
     * @param DataTransformer $dataTransformer
     */
    public function __construct(DataTransformer $dataTransformer)
    {
        $this->dataTransformer = $dataTransformer;
    }


    /**
     * @param string $queryString
     * @param bool $typeahead
     * @param int $limit
     * @return array
     */
    public function readByCode(
        string $queryString,
        bool $typeahead = false,
        int $limit = 20
    ): array {
        $accountIndexEloquentModel = AccountEloquentModel::selectByCode($queryString)->get();
        $accountIndexArray = $this->dataTransformer->transformData($accountIndexEloquentModel);

        return $accountIndexArray;
    }

    /**
     * @param string $queryString
     * @param string $coasCode
     * @param string $aTypeCode
     * @param string $nChgDate
     * @param string $dataEntryInd
     * @param bool $typeahead
     * @param int $limit
     * @return array
     */
    public function readByCodeTypeahead(
        string $queryString,
        string $coasCode,
        string $aTypeCode,
        string $nChgDate,
        string $dataEntryInd,
        bool $typeahead = false,
        int $limit = 20
    ): array {
        $accountIndexEloquentModel = AccountEloquentModel::selectTypeaheadByCodeOrTitle($queryString, $coasCode, $aTypeCode, $nChgDate, $dataEntryInd)->get();
        $accountIndexArray = $this->dataTransformer->transformData($accountIndexEloquentModel);

        return $accountIndexArray;
    }
}
