<?php

namespace MiamiOH\WSFinanceAccountCode\Repositories;

use MiamiOH\WSFinanceAccountCode\DataTransformers\DataTransformer;
use MiamiOH\WSFinanceAccountCode\EloquentModels\AccountIndexEloquentModel;

/**
 * Class AccountIndexRepositorySQL
 */
class AccountIndexRepositorySQL implements AccountIndexRepository
{
    private $dataTransformer = null;

    public function __construct(DataTransformer $dataTransformer)
    {
        $this->dataTransformer = $dataTransformer;
    }

    /**
     * @param string $queryString
     * @param bool $typeahead
     * @param int $limit
     * @return array
     */
    public function readByIndexCode(
        string $queryString,
        bool $typeahead = false,
        int $limit = 20
    ): array {
        if ($typeahead) {
            $accountIndexEloquentModel = AccountIndexEloquentModel::selectTypeaheadByIndexCodeOrTitle($queryString, $limit)->get();
        } else {
            $accountIndexEloquentModel = AccountIndexEloquentModel::selectByIndexCode($queryString)->get();
        }

        $accountIndexArray = $this->dataTransformer->transformData($accountIndexEloquentModel);

        return $accountIndexArray;
    }
}
