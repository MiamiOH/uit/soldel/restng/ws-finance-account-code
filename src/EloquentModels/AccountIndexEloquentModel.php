<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSFinanceAccountCode\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class AccountIndexEloquentModel extends Model
{

    protected $connection = 'MUWS_GEN_PROD';


    /**
     * @var string $table Table name
     */
    public $table = 'ftvacci';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'ftvacci_surrogate_id';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * @param Builder $query
     * @param string $accountIndexCode
     * @return Builder|null
     */
    public function scopeSelectByIndexCode(Builder $query, string $accountIndexCode)
    {
        if (!empty($accountIndexCode)) {
            $accountIndexCode = strtoupper($accountIndexCode);

            return $query->select(
                'ftvacci_title',
                'ftvacci_acci_code',
                'ftvacci_fund_code',
                'ftvacci_orgn_code',
                'ftvacci_prog_code',
                'ftvacci_coas_code',
                'ftvacci_acct_code'
            )
                ->whereRaw('UPPER("FTVACCI_ACCI_CODE") = ?', [$accountIndexCode])
                ->where('ftvacci_status_ind', 'A')
                ->where(function (Builder $query) {
                    $query->whereRaw('ftvacci_eff_date <= SYSDATE')->orWhereNull('ftvacci_eff_date');
                })
                ->where(function (Builder $query) {
                    $query->whereRaw('ftvacci_nchg_date > SYSDATE')->orWhereNull('ftvacci_nchg_date');
                });
        }

        return null;
    }

    /**
     * @param Builder $query
     * @param string $queryString
     * @param int $limit
     * @return Builder|null
     */
    public function scopeSelectTypeaheadByIndexCodeOrTitle(Builder $query, string $queryString, int $limit = 20)
    {
        if (!empty($queryString)) {
            $queryString = strtoupper($queryString);

            return $query->select(
                'ftvacci_title',
                'ftvacci_acci_code',
                'ftvacci_fund_code',
                'ftvacci_orgn_code',
                'ftvacci_prog_code',
                'ftvacci_coas_code',
                'ftvacci_acct_code'
            )
                ->selectRaw("ftvacci_title || ' (' || ftvacci_acci_code || ')' AS title_index_code")
                ->where('ftvacci_status_ind', 'A')
                ->where(function (Builder $query) use ($queryString) {
                    $query->whereRaw('UPPER("FTVACCI_ACCI_CODE") LIKE ?', ["%$queryString%"])
                          ->orWhereRaw('UPPER("FTVACCI_TITLE") LIKE ?', ["%$queryString%"]);
                })
                ->where(function (Builder $query) {
                    $query->whereRaw('ftvacci_eff_date <= SYSDATE')->orWhereNull('ftvacci_eff_date');
                })
                ->where(function (Builder $query) {
                    $query->whereRaw('ftvacci_nchg_date > SYSDATE')->orWhereNull('ftvacci_nchg_date');
                })
                ->limit($limit);
        }

        return null;
    }
}
