<?php

namespace MiamiOH\WSFinanceAccountCode\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class AccountEloquentModel extends Model
{

    protected $connection = 'MUWS_GEN_PROD';


    /**
     * @var string $table Table name
     */
    public $table = 'ftvacct';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'ftvacct_surrogate_id';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    protected $casts = [
        'ftvacct_nchg_date' => 'date',
    ];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    /**
     * @param Builder $query
     * @param string $accountCode
     * @return Builder|null
     */
    public function scopeSelectByCode(Builder $query, string $accountCode)
    {
        if (!empty($accountCode)) {
            $accountCode = strtoupper($accountCode);

            return $query->select(
                'ftvacct_title',
                'ftvacct_acct_code',
                'ftvacct_coas_code',
                'ftvacct_atyp_code',
                'ftvacct_data_entry_ind',
                'ftvacct_status_ind',
                'ftvacct_nchg_date',
                'ftvacct_acct_code_pred'
            )
                ->whereRaw('UPPER("FTVACCT_ACCT_CODE") = ?', [$accountCode])
                ->where('ftvacct_status_ind', 'A')
                ->where(function (Builder $query) {
                    $query->whereRaw('ftvacct_eff_date <= SYSDATE')->orWhereNull('ftvacct_eff_date');
                })
                ->where(function (Builder $query) {
                    $query->whereRaw('ftvacct_nchg_date > SYSDATE')->orWhereNull('ftvacct_nchg_date');
                });
        }
        return null;
    }


    public function scopeSelectTypeaheadByCodeOrTitle(Builder $query, string $queryString, string $coasCode, string $aTypeCode, string $nChgDate, string $dataEntryInd, int $limit = 20)
    {
        $queryString = strtoupper($queryString);
        $coasCode = strtoupper($coasCode);
        $aTypeCode = strtoupper($aTypeCode);
        $nChgDate = strtoupper($nChgDate);
        $dataEntryInd = strtoupper($dataEntryInd);


        if (!empty($queryString)) {
            $query = $query->where(function (Builder $query) use ($queryString) {
                $query->whereRaw('UPPER("FTVACCT_ACCT_CODE") LIKE ?', ["%$queryString%"])
                    ->orWhereRaw('UPPER("FTVACCT_TITLE") LIKE ?', ["%$queryString%"]);
            });
        }

        if (!empty($coasCode)) {
            $query = $query->where('ftvacct_coas_code', $coasCode);
        }

        if (!empty($aTypeCode)) {
            $query = $query->where('ftvacct_atyp_code', $aTypeCode);
        }

        if (!empty($nChgDate)) {
            $nChgDate = date('Y-m-d', strtotime($nChgDate));
            $query = $query->whereDate('ftvacct_nchg_date', '=', $nChgDate);
        }

        if (!empty($dataEntryInd)) {
            $query = $query->where('ftvacct_data_entry_ind', $dataEntryInd);
        }

        return $query->select(
            'ftvacct_title',
            'ftvacct_acct_code',
            'ftvacct_coas_code',
            'ftvacct_atyp_code',
            'ftvacct_data_entry_ind',
            'ftvacct_status_ind',
            'ftvacct_nchg_date',
            'ftvacct_acct_code_pred'
        )
            ->selectRaw("ftvacct_title || ' (' || ftvacct_acct_code || ')' AS account_title_code")
            ->where('ftvacct_status_ind', 'A')
            ->where(function (Builder $query) {
                $query->whereRaw('ftvacct_eff_date <= SYSDATE')->orWhereNull('ftvacct_eff_date');
            })
            ->where(function (Builder $query) {
                $query->whereRaw('ftvacct_nchg_date > SYSDATE')->orWhereNull('ftvacct_nchg_date');
            })
            ->limit($limit);
    }
}
