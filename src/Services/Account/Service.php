<?php


namespace MiamiOH\WSFinanceAccountCode\Services\Account;

use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\WSFinanceAccountCode\DataTransformers\FinanceAccountCodeDataTransformer;
use MiamiOH\WSFinanceAccountCode\Repositories\AccountRepository;
use MiamiOH\WSFinanceAccountCode\Repositories\AccountRepositorySQL;

class Service extends \MiamiOH\RESTng\Service
{
    protected $dataSource = 'MUWS_GEN_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var AccountRepository
     */
    protected $repository = null;


    public function getDependencies()
    {
        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $dataTransformer = new FinanceAccountCodeDataTransformer();
        $this->repository = new AccountRepositorySQL($dataTransformer);
    }

    /**
     * @throws \Exception
     */
    public function getSingle()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getSingle(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getTypeahead()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getTypeahead(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }
}
