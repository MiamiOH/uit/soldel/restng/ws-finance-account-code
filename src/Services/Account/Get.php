<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/26/18
 * Time: 10:39 AM
 */

namespace MiamiOH\WSFinanceAccountCode\Services\Account;

use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\WSFinanceAccountCode\Repositories\AccountRepository;
use MiamiOH\WSFinanceAccountCode\Utilities\Authorization;
use function trim;
use function var_dump;

class Get
{
    /**
     * @param Request $request
     * @param Response $response
     * @param AccountRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function getSingle(
        Request $request,
        Response $response,
        AccountRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $code = $options['accountCode'] ?? '';

        $code = trim($code);

        if (empty($code)) {
            throw new BadRequest('Missing required field: account code.n');
        }

        $record = $repository->readByCode($code);

        // DONE
        $response->setPayload($record);
        $response->setStatus($status);

        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param AccountRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function getTypeahead(
        Request $request,
        Response $response,
        AccountRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $queryString = isset($options['q']) ? trim($options['q']) : '';
        $coasCode = isset($options['chartOfAccountsCode']) ? trim($options['chartOfAccountsCode']) : '';
        $aTypeCode = isset($options['accountTypeCode']) ? trim($options['accountTypeCode']) : '';
        $nChgDate = isset($options['nextChangeDate']) ? trim($options['nextChangeDate']) : '';
        $dataEntryInd = isset($options['dataEntryIndicator']) ? trim($options['dataEntryIndicator']) : '';

        $limit = $request->getLimit();

        if (empty($options)) {
            $record = [];
        } else {
            $record = $repository->readByCodeTypeahead($queryString, $coasCode, $aTypeCode, $nChgDate, $dataEntryInd, true, $limit);
        }

        // DONE
        $response->setPayload($record);
        $response->setStatus($status);

        return $response;
    }
}
