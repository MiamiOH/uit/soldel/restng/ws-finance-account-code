<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/26/18
 * Time: 10:39 AM
 */

namespace MiamiOH\WSFinanceAccountCode\Services\AccountIndex;

use MiamiOH\RESTng\Exception\BadRequest;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\WSFinanceAccountCode\Repositories\AccountIndexRepository;
use MiamiOH\WSFinanceAccountCode\Utilities\Authorization;

class Get
{
    /**
     * @param Request $request
     * @param Response $response
     * @param AccountIndexRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function getSingle(
        Request $request,
        Response $response,
        AccountIndexRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $indexCode = $options['accountIndexCode'] ?? '';

        $indexCode = trim($indexCode);

        if (empty($indexCode)) {
            throw new BadRequest('Missing required field: index code.n');
        }

        $record = $repository->readByIndexCode($indexCode);

        // DONE
        $response->setPayload($record);
        $response->setStatus($status);

        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param AccountIndexRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function getTypeahead(
        Request $request,
        Response $response,
        AccountIndexRepository $repository
    ): Response {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $queryString = $options['q'] ?? '';

        $queryString = trim($queryString);

        $limit = $request->getLimit();

        if (empty($queryString)) {
            $record = [];
        } else {
            $record = $repository->readByIndexCode($queryString, true, $limit);
        }

        // DONE
        $response->setPayload($record);
        $response->setStatus($status);

        return $response;
    }
}
