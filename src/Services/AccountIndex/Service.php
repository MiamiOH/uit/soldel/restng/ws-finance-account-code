<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/24/18
 * Time: 3:05 PM
 */

namespace MiamiOH\WSFinanceAccountCode\Services\AccountIndex;

use MiamiOH\WSFinanceAccountCode\DataTransformers\FinanceAccountIndexCodeDataTransformer;
use MiamiOH\WSFinanceAccountCode\Repositories\AccountIndexRepository;
use MiamiOH\WSFinanceAccountCode\Utilities\Authorization;
use MiamiOH\WSFinanceAccountCode\Utilities\DateTime;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\WSFinanceAccountCode\Repositories\AccountIndexRepositorySQL;

class Service extends \MiamiOH\RESTng\Service
{
    protected $dataSource = 'MUWS_GEN_PROD';

    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var AccountIndexRepository
     */
    protected $repository = null;

    public function setDataSource(DataSource $dataSourceManager)
    {
        $dataSourceConfig = $dataSourceManager->getDataSource($this->dataSource);

        RESTngEloquentFactory::boot($dataSourceConfig);
    }

    public function getDependencies()
    {
        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $dataTransformer = new FinanceAccountIndexCodeDataTransformer();
        $this->repository = new AccountIndexRepositorySQL($dataTransformer);
    }

    /**
     * @throws \Exception
     */
    public function getSingle()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getSingle(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getTypeahead()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getTypeahead(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }
}
