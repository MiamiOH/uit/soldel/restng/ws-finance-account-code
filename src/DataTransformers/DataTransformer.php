<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/22/19
 * Time: 3:21 PM
 */

namespace MiamiOH\WSFinanceAccountCode\DataTransformers;

use Illuminate\Support\Collection;

abstract class DataTransformer
{
    abstract public function transformData(Collection $collection, array $options = []);
}
