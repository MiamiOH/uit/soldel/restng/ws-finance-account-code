<?php

namespace MiamiOH\WSFinanceAccountCode\DataTransformers;

use Illuminate\Support\Collection;

class FinanceAccountCodeDataTransformer extends DataTransformer
{
    private $keyExchanges = [
        'title' => 'ftvacct_title',
        'accountCode' => 'ftvacct_acct_code',
        'chartOfAccountsCode' => 'ftvacct_coas_code',
        'accountTypeCode' => 'ftvacct_atyp_code',
        'nextChangeDate' => 'ftvacct_nchg_date',
        'dataEntryIndicator' => 'ftvacct_data_entry_ind',
        'statusIndicator' => 'ftvacct_status_ind',
        'accountTitleCode' => 'account_title_code',
    ];

    /**
     * @param Collection $collection
     * @param array $options
     * @return array
     */
    public function transformData(Collection $collection, array $options = []): array
    {
        $result = [];

        foreach ($collection as $accountCode => $model) {
            $modelAttributes = $model->getAttributes();

            foreach ($this->keyExchanges as $key => $val) {
                if (isset($modelAttributes[$val])) {
                    $date = strtotime($modelAttributes['ftvacct_nchg_date']);
                    $modelAttributes['ftvacct_nchg_date'] = date('Y-m-d\TH:i:s', $date);

                    $result[$accountCode][$key] = $modelAttributes[$val];
                }
            }
        }
        return $result;
    }
}
