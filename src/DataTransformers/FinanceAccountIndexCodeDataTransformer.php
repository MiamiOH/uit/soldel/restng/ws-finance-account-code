<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 1/30/19
 * Time: 6:58 PM
 */

namespace MiamiOH\WSFinanceAccountCode\DataTransformers;

use Illuminate\Support\Collection;

class FinanceAccountIndexCodeDataTransformer extends DataTransformer
{
    private $keyExchanges = [
        'title' => 'ftvacci_title',
        'accountIndexCode' => 'ftvacci_acci_code',
        'fundCode' => 'ftvacci_fund_code',
        'organizationCode' => 'ftvacci_orgn_code',
        'programCode' => 'ftvacci_prog_code',
        'chartOfAccount' => 'ftvacci_coas_code',
        'accountCode' => 'ftvacci_acct_code',
        'titleIndexCode' => 'title_index_code',
    ];

    /**
     * @param Collection $collection
     * @param array $options
     * @return array
     */
    public function transformData(Collection $collection, array $options = []): array
    {
        $result = [];

        foreach ($collection as $index => $model) {
            $modelAttributes = $model->getAttributes();

            foreach ($this->keyExchanges as $key => $val) {
                if (isset($modelAttributes[$val])) {
                    $result[$index][$key] = $modelAttributes[$val];
                }
            }
        }

        return $result;
    }
}
