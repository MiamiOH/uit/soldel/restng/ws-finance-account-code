# Finance Account Code Web Service
A series of Finanace Account Code Web Services.

## Local Development

### Prerequisites
In order to start local development, please make sure following prerequisites are met:
- [Docker](https://docs.docker.com/install) installed
- [Docker Compose](https://docs.docker.com/compose/install)

### Setup

1. Clone the repo.  The "Run" commands below should be run from the project's directory.
1. Copy `docker/config/authorizations.yaml.example` to `docker/config/authorizations.yaml`.  Update the values if needed.
1. Copy `docker/config/credentials.yaml.example` to `docker/config/credentials.yaml`.  Update the passwords and tokens to your liking.
1. Copy `docker/config/datasources.yaml.example` to `docker/config/datasources.yaml`.  Update the passwords with the actual passwords to connect to Oracle development instances.  If you do not know them, see a application developer or DBA.
1. Run `docker-compose up -d` to start the containers.
1. Run `docker run -it --rm -v $(pwd):/opt/project -v ~/.ssh:/root/.ssh miamioh/php:7.3-phpstorm composer install` to install composer dependencies in the main project.  You may be prompted for your SSH id_rsa passhrase.
1. Access https://localhost/swagger-ui/ to confirm the containers and application are working.  The resources for this project should be listed.